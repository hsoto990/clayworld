package aasf;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;


public class LisWorld  {

	public static void initLisWorld(){
		for (int ix = 0; ix < bgw / 20; ix++) {
			cuad.add(new ArrayList<ArrayList<Integer>>());
			for (int iy = 0; iy < bgh / 20; iy++)
				cuad.get(ix).add(new ArrayList<Integer>());
		}

		makeSquare(400, 50, 5, 5, Color.cyan, 11, 100, 0, 0);
		makeSquare(300, 299, 50, 40, Color.pink, 10, 1, 0, 0);
		Tikamado.run();
	}
	
	private static ArrayList<ArrayList<ArrayList<Integer>>> cuad = new ArrayList<ArrayList<ArrayList<Integer>>>();

	public static ArrayList<Particle> parjs = new ArrayList<Particle>();

	private static int Psize = 0;
	final private static int bgw = 1700;
	final private static int bgh = 1000;

	private static ArrayList<Integer> ln1 = new ArrayList<Integer>();
	private static ArrayList<Integer> ln2 = new ArrayList<Integer>();


	

	static Color clr = new Color(150, 150, 160, 50);

	static void makeSquare(int x, int y, int anc, int alt, Color saaa, int sep, float mas, float fx, float fy) {
		for (int iy = 0; iy < alt; iy++) {
			for (int ix = 0; ix < anc; ix++) {
				Padd(x + ix * sep, y + iy * sep, fx, fy, mas, saaa);
				if (ix != 0) {
					ln1.add(Psize - 2);
					ln2.add(Psize - 1);
				}
				if (iy != 0) {
					ln1.add(Psize - anc - 1);
					ln2.add(Psize - 1);

				}
			}

		}
	}

	void makeFluidSquare(int x, int y, int anc, int alt, Color saaa, int sep, float mas) {
		for (int iy = 0; iy < alt; iy++) {
			for (int ix = 0; ix < anc; ix++) {
				Padd(x + ix * sep, y + iy * sep, 0d, 0d, mas, saaa);
			}
		}
	}
	
	

	public static void Draw(Graphics g) {
		

			g.setColor(clr);
			g.fillRect(0, 0, bgw / 2, bgh / 2);

			for (int i = 0; i < Psize; i++) {
				g.setColor(parjs.get(i).cr);
				g.fillRect(parjs.get(i).px.intValue() / 2, parjs.get(i).py.intValue() / 2, 4, 4);
			}
			// System.out.println("a");

		}

	public static void Padd(double vpx, double vpy, double vfx, double vfy, float vms, Color vcr) {
		parjs.add(new Particle());
		parjs.get(Psize).set(vpx, vpy, vfx, vfy, vms, vcr);
		cuad.get((int) (vpx / 20)).get((int) (vpy / 20)).add(Psize);
		Psize++;
	}

	public static void mover() {

		for (int i = 0; i < Psize; i++) {
			double xp = parjs.get(i).px;
			double yp = parjs.get(i).py;
			int cox = (int) (xp / 20);
			int coy = (int) (yp / 20);

			for (int cy = -1; cy < 2; cy++) {
				for (int cx = -1; cx < 2; cx++) {

					try {
						ArrayList<Integer> Sape = cuad.get(cox + cx).get(coy + cy);
						int Ssize = Sape.size();
						for (int f = Ssize - 1; f >= 0; f--) {
							int sf = Sape.get(f);
							if (sf>i) {
							double disx = xp - parjs.get(sf).px;
							double disy = yp - parjs.get(sf).py;
							double dis = disx * disx + disy * disy;

							if (dis < 400d & dis != 0) {
								double promx = (parjs.get(sf).fx + parjs.get(i).fx) / 2;
								double promy = (parjs.get(sf).fy + parjs.get(i).fy) / 2;

								double mod1=0.01;
								int mod2=1;
								
								parjs.get(i).fx = (promx*mod1 + parjs.get(i).fx*mod2) / (mod1+mod2);
								parjs.get(i).fy = (promy*mod1 + parjs.get(i).fy*mod2) / (mod1+mod2);
								parjs.get(sf).fx = (promx*mod1 + parjs.get(sf).fx*mod2) / (mod1+mod2);
								parjs.get(sf).fy = (promy*mod1 + parjs.get(sf).fy*mod2) / (mod1+mod2);
							}

						}}
					} catch (Exception e) {
					}

				}
			}
		}
		
		// REPULSION
		for (int i = 0; i < Psize; i++) {
			double xp = parjs.get(i).px;
			double yp = parjs.get(i).py;
			int cox = (int) (xp / 20);
			int coy = (int) (yp / 20);
			cuad.get(cox).get(coy).remove(cuad.get(cox).get(coy).indexOf(i));

			for (int cy = -1; cy < 2; cy++) {
				for (int cx = -1; cx < 2; cx++) {

					try {
						ArrayList<Integer> Sape = cuad.get(cox + cx).get(coy + cy);
						int Ssize = Sape.size();
						for (int f = Ssize - 1; f >= 0; f--) {
							int sf = Sape.get(f);
							double disx = xp - parjs.get(sf).px;
							double disy = yp - parjs.get(sf).py;
							double dis = disx * disx + disy * disy;

							if (dis < 400d & dis != 0) {
								dis = Math.sqrt(dis);
								double Force = (20d - dis) * (parjs.get(i).ms + parjs.get(sf).ms);
								double XForce = Force * disx / dis;
								double YForce = Force * disy / dis;

								parjs.get(i).fx += XForce;
								parjs.get(i).fy += YForce;
								parjs.get(sf).fx -= XForce;
								parjs.get(sf).fy -= YForce;
							}

						}
					} catch (Exception e) {
					}

				}
			}
		}
		
		

		// ATRACCION
		int o = 0;
		while (o < ln1.size()) {
			int i1 = ln1.get(o);
			int i2 = ln2.get(o);
			double disx = parjs.get(i1).px - parjs.get(i2).px;
			double disy = parjs.get(i1).py - parjs.get(i2).py;
			double dis = disx * disx + disy * disy;

			if (dis < 900d & dis != 0) {

				dis = Math.sqrt(dis);
				double Force = (dis) * (parjs.get(i1).ms + parjs.get(i2).ms);
				double XForce = Force * disx / dis;
				double YForce = Force * disy / dis;

				parjs.get(i1).fx -= XForce;
				parjs.get(i1).fy -= YForce;
				parjs.get(i2).fx += XForce;
				parjs.get(i2).fy += YForce;
				o++;

			} else {
				ln1.remove(o);
				ln2.remove(o);
			}

		}
		
		// MOVIMIENTO
		for (int i = 0; i < Psize; i++) {
			float mas = parjs.get(i).ms;
			double cx = parjs.get(i).px;
			double cy = parjs.get(i).py;

			// GRAVEDAD
//			parjs.get(i).fy += .3 * mas;

			parjs.get(i).fx*=0.98;
			parjs.get(i).fy*=0.98;
			
			parjs.get(i).px = cx + parjs.get(i).fx / mas / 100 + (Math.random()-0.5)/10;
			parjs.get(i).py = cy + parjs.get(i).fy / mas / 100 + (Math.random()-0.5)/10;

			if (parjs.get(i).py > bgh - 40d) {
				parjs.get(i).py = bgh - 40d;
				if (parjs.get(i).fy > 0)
					parjs.get(i).fy = -parjs.get(i).fy;
			} else if (parjs.get(i).py < 40d) {
				parjs.get(i).py = 40d;
				if (parjs.get(i).fy < 0)
					parjs.get(i).fy = -parjs.get(i).fy;
			}
			if (parjs.get(i).px > bgw - 40d) {
				parjs.get(i).px = bgw - 40d;
				if (parjs.get(i).fx > 0)
					parjs.get(i).fx = -parjs.get(i).fx;
			} else if (parjs.get(i).px < 40d) {
				parjs.get(i).px = 40d;
				if (parjs.get(i).fx < 0)
					parjs.get(i).fx = -parjs.get(i).fx;
			}
			cuad.get((int) (parjs.get(i).px / 20)).get((int) (parjs.get(i).py / 20)).add(i);
		}

	}
}
