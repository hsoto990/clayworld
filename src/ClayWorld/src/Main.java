package aasf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.KeyAdapter;

public class Main extends JPanel {

	public Main() {
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				Controles.playerPressOrRelease(true,arg0);
			}
			public void keyReleased(KeyEvent arg0) {
				Controles.playerPressOrRelease(false,arg0);
			}
		});
	}

	private static final long serialVersionUID = 1L;
	private static final int RECT_X = 0;
	private static final int RECT_Y = RECT_X;
	private static final int RECT_WIDTH = 530;
	private static final int RECT_HEIGHT = RECT_WIDTH;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		LisWorld.Draw(g);
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(RECT_WIDTH*2 + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	}

	public static JFrame frame = new JFrame("DrawRect");

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {

		Main mainPanel = new Main();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		LisWorld.initLisWorld();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}