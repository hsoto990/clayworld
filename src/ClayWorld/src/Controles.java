package aasf;

import java.awt.Color;
import java.awt.event.KeyEvent;

public class Controles {

	static boolean[] teclas = new boolean[5];

	static void playerPressOrRelease(boolean trofal, KeyEvent arg0) {
		if (teclaPressed(arg0, "W"))
			teclas[0] = trofal;
		if (teclaPressed(arg0, "A"))
			teclas[1] = trofal;
		if (teclaPressed(arg0, "S"))
			teclas[2] = trofal;
		if (teclaPressed(arg0, "D"))
			teclas[3] = trofal;
		if (teclaPressed(arg0, "R"))
			teclas[4] = trofal;
	}

	static boolean teclaPressed(KeyEvent arg0, String w) {
		return ("" + arg0.getKeyChar()).equalsIgnoreCase(w);
	}
	static double speed = 2;

	static void ForTimerendo() {
		if (teclas[0]) {
			for (int i=0;i<25;i++)
				LisWorld.parjs.get(i).fy -= speed*LisWorld.parjs.get(i).ms;
		}
		if (teclas[1])
			for (int i=0;i<25;i++)
				LisWorld.parjs.get(i).fx -= speed*LisWorld.parjs.get(i).ms;
		if (teclas[2]) {
			for (int i=0;i<25;i++)
				LisWorld.parjs.get(i).fy += speed*LisWorld.parjs.get(i).ms;
		}
		if (teclas[3])
			for (int i=0;i<25;i++)
				LisWorld.parjs.get(i).fx += speed*LisWorld.parjs.get(i).ms;
	}

}
