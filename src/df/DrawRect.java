package df;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

public class DrawRect extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final int RECT_X = 0;
	private static final int RECT_Y = RECT_X;
	private static final int RECT_WIDTH = 530;
	private static final int RECT_HEIGHT = RECT_WIDTH;

	static boolean[][] grid = new boolean[17][17];

	static Graphics ham;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int ix = 0; ix < 17; ix++) {
			for (int iy = 0; iy < 17; iy++) {
				if (grid[ix][iy])
					g.setColor(new Color(150, 150, 150));
				else
					g.setColor(new Color(0, 0, 0));
			g.fillRect(ix * 20, iy * 20, 20, 20);
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	}

	public static JFrame frame = new JFrame("DrawRect");

	static ArrayList<Point> blocks = new ArrayList<Point>();

	static void repaintLoop() {
		new Thread() {
			public void run() {
				while (true) {
					for (int ix = 0; ix < 17; ix++)
						for (int iy = 0; iy < 17; iy++)
							grid[ix][iy] = false;

					for (int ix = -7; ix < 9; ix++) {
						double e = 0d;
						for (int i = 1; i < 9; i++) {
							e += ix / 8d;
							int x = (int) (8 + e);
							int y = 8 + i;
							grid[x][y]=true;
							if (blocks.contains(new Point(x, y)))
								i = 9;
						}
					}
					for (int ix = -7; ix < 9; ix++) {
						double e = 0d;
						for (int i = 1; i < 9; i++) {
							e += ix / 8d;
							int x = (int) (8 - e);
							int y = 8 - i;
							grid[x][y]=true;
							if (blocks.contains(new Point(x, y)))
								i = 9;
						}
					}
					for (int ix = -7; ix < 9; ix++) {
						double e = 0d;
						for (int i = 1; i < 9; i++) {
							e += ix / 8d;
							int x = 8 + i;
							int y = (int) (8 - e);
							grid[x][y]=true;
							if (blocks.contains(new Point(x, y)))
								i = 9;
						}
					}
					for (int ix = -7; ix < 9; ix++) {
						double e = 0d;
						for (int i = 1; i < 9; i++) {
							e += ix / 8d;
							int x = 8 - i;
							int y = (int) (8 + e);
							grid[x][y]=true;
							if (blocks.contains(new Point(x, y)))
								i = 9;
						}
					}

					frame.repaint();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}

				}
			}
		}.start();
	}

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {
		blocks.add(new Point(3, 3));
		blocks.add(new Point(4, 3));
		blocks.add(new Point(5, 4));
		blocks.add(new Point(6, 4));
		blocks.add(new Point(7, 4));

		blocks.add(new Point(6, 13));
		blocks.add(new Point(3, 13));
		repaintLoop();

		DrawRect mainPanel = new DrawRect();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}