package df;

import java.awt.Color;

public class Particle {
	Double px = .0;
	Double py = .0;
	Double fx = .0;
	Double fy = .0;
	Float ms = 0f;
	Color cr;
	void set(Double vpx,Double vpy,Double vfx,Double vfy,float vms,Color vcr) {
		px = vpx;
		py = vpy;
		fx = vfx;
		fy = vfy;
		ms = vms;
		cr = vcr;
	}
	
	
}
